import sys
import socket
import os
import email.utils
import mimetypes

from email.utils import formatdate

def server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server_address = ('194.29.175.240', 19991) #194.29.175.240
    server_socket.bind(server_address)

    server_socket.listen(1)

    #html = open('web/web_page.html').read()

    try:
        polacz(server_socket)

    finally:
        server_socket.close()

def polacz(server_socket):
    print('Serwer wystartowal!')
    while True:
        print('Czekam na polaczenie...')
        conn, client = server_socket.accept()
        print('Polaczono z {0}:{1}'.format(*client))

        try:
            request = conn.recv(1024)
            odpowiedz = stworz_odpowiedz(request)
            conn.sendall(odpowiedz)
        except:
            blad_servera()
        finally:
            conn.close()

def stworz_odpowiedz(request):
    try:
        podzielone = request.split('\r\n')
        rodzaj = podzielone[0].split(' ')[0]
        adres = 'web/' + (podzielone[0].split(' ')[1])[1:]

        if(rodzaj == 'GET'):
            if os.path.isdir(adres):
                strona = otworz_strone()
                strona += '<h1>Pliki w folderze /' + adres +':</h1>'
                strona += '<ul>'
                if(adres == 'web/'):
                    znak = ''
                else:
                    znak = '/'
                for plik in os.listdir(adres):
                    strona += '<li><a href="' + znak + adres.replace('web/', "")  + '/' +  plik + '">' + plik + '</a></li>'
                strona += '</ul>'
                strona += zamknij_strone()
                return sklej('200 OK', 'text/html', strona)
            else:
                if(os.path.exists(adres)):
                    return sklej('200 OK', mimetypes.guess_type(adres)[0], open(adres, 'rb').read())
                else:
                    msg = '404 Not Found'
                    return sklej(msg, 'text/html', zwroc_strone_bledu(msg))
        else:
            msg = '405 Method Not Allowed'
            return sklej(msg, 'text/html', zwroc_strone_bledu(msg))
    except:
        blad_servera()

def blad_servera():
    msg = '500 Internal Server Error'
    return sklej(msg, 'text/html', zwroc_strone_bledu(msg))

def zwroc_strone_bledu(tekst):
    return otworz_strone() + '<h1>' + tekst + '</h1>' + zamknij_strone()

def otworz_strone():
    return '<!DOCTYPE html><html><head><meta charset="utf-8" /></head><body>'
def zamknij_strone():
    return '</body></html>'

def sklej(n, rodzaj, html):
    naglowek = 'HTTP/1.1 ' + n + '\r\n'
    contnent_type = 'Content-Type: '+rodzaj+'\r\n'
    data = 'GMT-Date: '+ email.utils.formatdate()+'\r\n'
    dlugosc = 'Content_Length: '+ str(len(html))+'\r\n'
    return naglowek + contnent_type + data + dlugosc +'\r\n' + html

if __name__ == '__main__':
    server()
    sys.exit(0)