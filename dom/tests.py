__author__ = 'Adam'

import unittest
import httplib
from server import stworz_odpowiedz

class HTTPServerTests(unittest.TestCase):

    server_address = ('194.29.175.240', 19991) #194.29.175.240

    def zwroc_informacje(self, metoda, sciezka):
        conn = httplib.HTTPConnection(self.server_address[0], self.server_address[1])
        conn.request(metoda, sciezka)
        return conn.getresponse()

    def sprawdz_poprawnosc(self, wynik, status, reason, header):
        self.assertEqual(status, wynik.status)
        self.assertEqual(reason, wynik.reason)
        czy_jest = False
        for h in wynik.getheaders():
            if header == h[1] and h[0] == 'content-type':
                czy_jest = True
                break
        self.assertEqual(czy_jest, True)

    def test_strona_glowna(self):
        wynik = self.zwroc_informacje('GET', '')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html')

    def test_zly_adres1(self):
        wynik = self.zwroc_informacje('GET', 'zly')
        self.sprawdz_poprawnosc(wynik, 404, 'Not Found', 'text/html')

    def test_zly_adres2(self):
        wynik = self.zwroc_informacje('GET', '/images/asdasd')
        self.sprawdz_poprawnosc(wynik, 404, 'Not Found', 'text/html')

    def test_images(self):
        wynik = self.zwroc_informacje('GET', '/images')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/html')

    def test_obrazek_1(self):
        wynik = self.zwroc_informacje('GET', '/images/gnu_meditate_levitate.png')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'image/png')

    def test_obrazek_2(self):
        wynik = self.zwroc_informacje('GET', '/images/jpg_rip.jpg')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'image/jpeg')

    def test_obrazek_3(self):
        wynik = self.zwroc_informacje('GET', '/images/obrazek.jpg')
        self.sprawdz_poprawnosc(wynik, 404, 'Not Found', 'text/html')

    def test_lokomotywa(self):
        wynik = self.zwroc_informacje('GET', '/lokomotywa.txt')
        self.sprawdz_poprawnosc(wynik, 200, 'OK', 'text/plain')

    def test_tekst_nieistniejacy(self):
        wynik = self.zwroc_informacje('GET', '/wiersz.txt')
        self.sprawdz_poprawnosc(wynik, 404, 'Not Found', 'text/html')

    def test_POST(self):
        wynik = self.zwroc_informacje('POST', '')
        self.sprawdz_poprawnosc(wynik, 405, 'Method Not Allowed', 'text/html')

if __name__ == '__main__':
    unittest.main()