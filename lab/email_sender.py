# -*- coding: utf-8 -*-

import smtplib

def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika

    nadawca = raw_input('Nadwaca: ')
    adresat = raw_input('Adresat: ')
    temat = raw_input('Temat: ')
    tresc = raw_input('Tresc: ' )

    # Stworzenie wiadomości

    template = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
    headers = template % (nadawca, adresat, temat)
    email_body = headers + tresc

    try:
        # Połączenie z serwerem pocztowym
        # Ustawienie parametrów

        server = smtplib.SMTP('194.29.175.240', 25)
        server.set_debuglevel(True)
        server.ehlo()

        # Autentykacja
        server.starttls()
        server.ehlo()
        server.login('p8', 'p8')

        # Wysłanie wiadomości

        server.sendmail(nadawca, adresat, email_body)
        pass

    finally:
        # Zamknięcie połączenia
        server.close()
        pass


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input('Wysłać jeszcze jeden list? ')
